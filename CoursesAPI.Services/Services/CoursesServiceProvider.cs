﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http.Headers;
using System.Threading;
using CoursesAPI.Models;
using CoursesAPI.Services.DataAccess;
using CoursesAPI.Services.Exceptions;
using CoursesAPI.Services.Models.Entities;


namespace CoursesAPI.Services.Services
{
	public class CoursesServiceProvider
	{
		private readonly IUnitOfWork _uow;

		private readonly IRepository<CourseInstance> _courseInstances;
		private readonly IRepository<TeacherRegistration> _teacherRegistrations;
		private readonly IRepository<CourseTemplate> _courseTemplates; 
		private readonly IRepository<Person> _persons;

		public CoursesServiceProvider(IUnitOfWork uow)
		{
			_uow = uow;

			_courseInstances      = _uow.GetRepository<CourseInstance>();
			_courseTemplates      = _uow.GetRepository<CourseTemplate>();
			_teacherRegistrations = _uow.GetRepository<TeacherRegistration>();
			_persons              = _uow.GetRepository<Person>();
		}

		/// <summary>
		/// You should implement this function, such that all tests will pass.
		/// </summary>
		/// <param name="courseInstanceID">The ID of the course instance which the teacher will be registered to.</param>
		/// <param name="model">The data which indicates which person should be added as a teacher, and in what role.</param>
		/// <returns>Should return basic information about the person.</returns>
		public PersonDTO AddTeacherToCourse(int courseInstanceID, AddTeacherViewModel model)
		{
            //### 1. Validation

            // Does the course exist?
            var course = _courseInstances.All().SingleOrDefault(x => x.ID == courseInstanceID);

            if (course == null)
            {
                throw new AppObjectNotFoundException(ErrorCodes.INVALID_COURSEINSTANCEID);
            }

            // Does the teacher exist?
            var teacher = _persons.All().SingleOrDefault(x => x.SSN == model.SSN);
            if (teacher == null)
            {
                throw new AppObjectNotFoundException();
            }

            // Is the teacher already teaching the course?
            var isTeaching = _teacherRegistrations.All().SingleOrDefault
                (x => (x.CourseInstanceID == courseInstanceID) &&
                      (x.SSN == model.SSN));

            if (isTeaching != null)
            {
                throw new AppValidationException("PERSON_ALREADY_REGISTERED_TEACHER_IN_COURSE");
            }


            // Does the course already have a main teacher?
            var mainTeacher = _teacherRegistrations.All().SingleOrDefault
                (x => (x.CourseInstanceID == courseInstanceID) &&
                    (x.Type == TeacherType.MainTeacher));

            if (mainTeacher != null && model.Type == TeacherType.MainTeacher)
            {
                throw new AppValidationException("COURSE_ALREADY_HAS_A_MAIN_TEACHER");
            }

            //### 2. Get neccesery info and add the teacher

            var myTeacher = new TeacherRegistration
            {
                SSN = model.SSN,
                CourseInstanceID = course.ID,
                Type = model.Type
            };

            _teacherRegistrations.Add(myTeacher);
            _uow.Save();

            //### 3. Figure out what to return

            var returnValue = new PersonDTO
            {
                Name = teacher.Name,
                SSN = teacher.SSN
            };

            return returnValue;
		}

	    /// <summary>
	    /// You should write tests for this function. You will also need to
	    /// modify it, such that it will correctly return the name of the main
	    /// teacher of each course.
	    /// </summary>
	    /// <param name="lang">Preferred language of the request, if any</param>
	    /// <param name="semester">Selected semester of the request, if any</param>
	    /// <param name="page">1-based index of the requested page.</param>
	    /// <returns>A list of course instances</returns>
	    public Envelope<List<CourseInstanceDTO>> GetCourseInstancesBySemester
            (HttpHeaderValueCollection<StringWithQualityHeaderValue> lang = null,
            string semester = null,
            int page = 1)
		{
			if (string.IsNullOrEmpty(semester))
			{
				semester = "20153";
			}

	        const int itemsPerPage = 10;

            // We only support Icelandic and English.
            var isIcelandic = true;

            // If the first preferred language of the client is English, then we use English, otherwise Icelandic
            if (lang != null)
            {
                if (lang.First().ToString().StartsWith("en"))
                {
                    isIcelandic = false;
                }
            }

            // Get course instance DTO info
			var courses = (from c in _courseInstances.All()
				           join ct in _courseTemplates.All() on c.CourseID equals ct.CourseID
				           where c.SemesterID == semester
				           select new CourseInstanceDTO
				           {
					           Name               =  isIcelandic? ct.Name : ct.NameEN,
					           TemplateID         = ct.CourseID,
					           CourseInstanceID   = c.ID,
					           MainTeacher        = ""
				           }).OrderBy(c => c.CourseInstanceID).Skip((page - 1) * itemsPerPage).Take(itemsPerPage).ToList();

            // Find the main teacher in course
            foreach (var c in courses)
            {
                var mainTeacher = (from reg in _teacherRegistrations.All()
                                   join per in _persons.All() on reg.SSN equals per.SSN
                                   where (reg.CourseInstanceID == c.CourseInstanceID) &&
                                         (reg.Type == TeacherType.MainTeacher)
                                   select new PersonDTO
                                   {
                                       Name = per.Name,
                                       SSN = per.SSN
                                   }).SingleOrDefault();

                if (mainTeacher != null)
                {
                    c.MainTeacher = mainTeacher.Name;
                }
            }

            // Assemble info for the envelope:

            // Get total courses in the current request
            var totalCourses = (from c in _courseInstances.All()
                                join ct in _courseTemplates.All() on c.CourseID equals ct.CourseID
                                where c.SemesterID == semester
                                select c).Count();

            // How many pages there are in total
            var pageCount = (int)(Math.Ceiling(((double) totalCourses) / ((double) itemsPerPage)));
            
            // Create the envelope object
	        var env = new Envelope<List<CourseInstanceDTO>>
	        {
	            Items = courses,
	            Paging = new Envelope<List<CourseInstanceDTO>>.PagingInfo
	            {
	                PageCount = pageCount,
	                PageNumber = page,
	                PageSize = itemsPerPage,
	                TotalNumberOfItems = totalCourses
	            }
	        };
            
            return env;
		}
	}
}
