﻿namespace CoursesAPI.Models
{
    /// <summary>
    /// This class is a container for generic types, holding info on paging system.
    /// </summary>
    /// <typeparam name="T">Type of items in a collection to keep track of</typeparam>
    public class Envelope<T>
    {
        /// <summary>
        /// Collection of items in the envelope
        /// </summary>
        public T Items { get; set; }

        /// <summary>
        /// Class containing all paging info
        /// </summary>
        public class PagingInfo
        {
            /// <summary>
            /// Stores the number of pages
            /// </summary>
            public int PageCount { get; set; }

            /// <summary>
            /// The number of items in each page
            /// </summary>
            public int PageSize { get; set; }

            /// <summary>
            /// 1-based index of the current page being returned
            /// </summary>
            public int PageNumber { get; set; }

            /// <summary>
            /// Total number of items in the collection
            /// </summary>
            public int TotalNumberOfItems { get; set; }
        }

        /// <summary>
        /// The object containg the paging info
        /// </summary>
        public PagingInfo Paging { get; set; }
    }
}
